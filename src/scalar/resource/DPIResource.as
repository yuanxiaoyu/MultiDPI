package scalar.resource
{
	import flash.utils.ByteArray;
	
	import scalar.core.DPICore;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;

	/**
	 * 
	 * @author hanxianming
	 * 
	 */	
	public final class DPIResource
	{
		private static var _instance:DPIResource;
		private var assets:AssetManager;
		
		private var dpiURL:String = "1x";
		
		public function DPIResource()
		{
			init();
		}
		
		
		public static function get instance():DPIResource
		{
			if (_instance == null)
				_instance = new DPIResource();
			
			return _instance;
		}
		
		private function init():void
		{
			assets = new AssetManager();
			var data:*;
			var atlasXML:XML;
			switch(DPICore.scale)
			{
				case 1:
//					data = new GameResource.compressed_texture_1x();
					data = new GameResource.png_texture_1x();
					atlasXML = new XML(new GameResource.atlas_compressed_texture_1x());
//					assets.addTextureAtlas("1x_compressed_texture", new TextureAtlas(Texture.fromAtfData(data, 1, false), atlasXML));
					assets.addTextureAtlas("1x_png_texture", new TextureAtlas(Texture.fromBitmap(data, false), atlasXML));
					break;
				case 2:
//					data = new GameResource.compressed_texture_2x();
					data = new GameResource.png_texture_2x();
					atlasXML = new XML(new GameResource.atlas_compressed_texture_2x());
//					assets.addTextureAtlas("2x_compressed_texture", new TextureAtlas(Texture.fromAtfData(data, 1, false), atlasXML));
					assets.addTextureAtlas("1x_png_texture", new TextureAtlas(Texture.fromBitmap(data, false), atlasXML));
					break;
				default:
					break;
			}
		}
		
		public function getTexture(name:String):Texture
		{
			return assets.getTexture(name);
		}
		
	}
}