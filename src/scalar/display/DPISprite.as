package scalar.display
{
	import scalar.core.DPICore;
	import scalar.core.IDPIObject;
	
	import starling.display.Sprite;

	/**
	 * 实现IDPIObject的Sprite对象。 
	 * @author hanxianming
	 * 
	 */	
	public class DPISprite extends Sprite implements IDPIObject
	{
		private var _dpiX:Number = 0;
		private var _dpiY:Number = 0;
		private var _dpiW:Number = NaN;
		private var _dpiH:Number = NaN;
		private var _dpiScaleX:Number = 1;
		private var _dpiScaleY:Number = 1;
		
		public function DPISprite()
		{
		}
		
		public function get dpiX():Number
		{
			return _dpiX;
		}
		
		public function set dpiX(value:Number):void
		{
			if (value == _dpiX)
				return;
			
			_dpiX = value;
			
			x = DPICore.scale * _dpiX;
		}
		
		public function get dpiY():Number
		{
			return _dpiY;
		}
		
		public function set dpiY(value:Number):void
		{
			if (value == _dpiY)
				return;
			
			_dpiY = value;
			
			y = DPICore.scale * _dpiY;
		}
		
		public function get dpiW():Number
		{
			if (isNaN(_dpiW))
				return width / DPICore.scale;
			return _dpiW;
		}
		
		public function set dpiW(value:Number):void
		{
			if (value == _dpiW)
				return;
			
			_dpiW = value;
			
			width = DPICore.scale * _dpiW;
		}
		
		public function get dpiH():Number
		{
			if (isNaN(_dpiH))
				return height / DPICore.scale;
			return _dpiH;
		}
		
		public function set dpiH(value:Number):void
		{
			if (value == _dpiH)
				return;
			
			_dpiH = value;
			
			height = DPICore.scale * _dpiH;
		}
		
		public function get dpiScaleX():Number
		{
			return _dpiScaleX;
		}
		
		public function set dpiScaleX(value:Number):void
		{
			if (value == _dpiScaleX)
				return;
			
			_dpiScaleX = value;
			
			_dpiW = _dpiW * _dpiScaleX;
			
			scaleX = _dpiScaleX;
		}
		
		public function get dpiScaleY():Number
		{
			return _dpiScaleY;
		}
		
		public function set dpiScaleY(value:Number):void
		{
			if (value == _dpiScaleY)
				return;
			
			_dpiScaleY = value;
			
			_dpiH = _dpiH * _dpiScaleY;
			
			scaleY = _dpiScaleY;
		}
		
		public function dpiXY(px:Number, py:Number):void
		{
			dpiX = px;
			dpiY = py;
		}
		
		public function dpiWH(pw:Number, ph:Number):void
		{
			dpiW = pw;
			dpiH = ph;
		}
		
		public function dpiScaleXY(sx:Number, sy:Number):void
		{
			dpiScaleX = sx;
			dpiScaleY = sy;
		}
	}
}