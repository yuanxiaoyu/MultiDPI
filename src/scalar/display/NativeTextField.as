package scalar.display
{
	import flash.text.TextField;
	
	import scalar.core.DPICore;
	/**
	 * 原生的textField文本，转化dpi系数。 
	 * @author hanxianming
	 * 
	 */	
	public class NativeTextField extends TextField
	{
		public function NativeTextField()
		{
			super();
		}
		
		override public function get width():Number
		{
			return super.width * DPICore.scale;
		}
		override public function set width(value:Number):void
		{
			super.width = value / DPICore.scale;
		}
		
		override public function get height():Number
		{
			return super.height * DPICore.scale;
		}
		override public function set height(value:Number):void
		{
			super.height = value / DPICore.scale;
		}
		
		override public function get textWidth():Number
		{
			return super.textWidth * DPICore.scale;
		}
		override public function get textHeight():Number
		{
			return super.textHeight * DPICore.scale;
		}
	}
}