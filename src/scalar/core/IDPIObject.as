package scalar.core
{
	/**
	 * dpi接口，实现此接口可实现不同DPI设备的布局处理。
	 * 你可以设置实现此接口对象的位置、尺寸和缩放信息。
	 * 设置之后这些信息会根据DPICore的scale的值会动态的进行调整。
	 * @author hanxianming
	 */	
	public interface IDPIObject
	{
		/**
		 * 设置和获取dpi对象的x位置信息。
		 */		
		function get dpiX():Number;
		function set dpiX(value:Number):void;
		
		/**
		 * 设置和获取dpi对象的y位置信息。
		 */	
		function get dpiY():Number;
		function set dpiY(value:Number):void;
		
		/**
		 * 设置和获取dpi对象的width宽度信息。
		 */	
		function get dpiW():Number;
		function set dpiW(value:Number):void;
		
		/**
		 * 设置和获取dpi对象的height高度信息。
		 */	
		function get dpiH():Number;
		function set dpiH(value:Number):void;
		
		/**
		 * 设置和获取dpi对象的scaleX缩放信息。
		 */	
		function get dpiScaleX():Number;
		function set dpiScaleX(value:Number):void;
		
		/**
		 * 设置和获取dpi对象的scaleY缩放信息。
		 */	
		function get dpiScaleY():Number;
		function set dpiScaleY(value:Number):void;
		
		/**
		 * 设置dpi对象的位置x坐标和y坐标。 
		 * @param px x坐标位置。
		 * @param py y坐标位置。
		 */		
		function dpiXY(px:Number, py:Number):void;
		
		/**
		 * 设置dpi对象的 宽度和高度信息。
		 * @param pw 宽度。
		 * @param ph 高度。
		 */		
		function dpiWH(pw:Number, ph:Number):void;
		
		/**
		 * 设置dpi对象的x轴向和y轴向的缩放比例。
		 * @param sx x轴向的缩放比例。
		 * @param sy y轴向的缩放比例。
		 */		
		function dpiScaleXY(sx:Number, sy:Number):void;
	}
}