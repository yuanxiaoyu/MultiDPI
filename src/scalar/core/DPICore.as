package scalar.core
{
	/**
	 * DPI核心 
	 * @author hanxianming
	 * 
	 */	
	public final class DPICore
	{
		/** 需要设置分辨率缩放比才能适配到不同分辨率的设备。*/
		public static var scale:Number = 1;
		public function DPICore()
		{
		}
		
		/**
		 * 设置对象的真实x值 。
		 * @param object 显示对象。
		 * @param value x值。
		 */		
		public static function x(object:Object, value:Number):void
		{
			object.x = dpiValue(value);
		}
		/**
		 * 设置对象的真实y值 。
		 * @param object 显示对象。
		 * @param value y值。
		 */	
		public static function y(object:Object, value:Number):void
		{
			object.y = dpiValue(value);
		}
		/**
		 * 设置对象的真实width值 。
		 * @param object 显示对象。
		 * @param value width值。
		 */	
		public static function width(object:Object, value:Number):void
		{
			object.width = dpiValue(value);
		}
		/**
		 * 设置对象的真实height值 。
		 * @param object 显示对象。
		 * @param value height值。
		 */	
		public static function height(object:Object, value:Number):void
		{
			object.height = dpiValue(value);
		}
		/**
		 * 设置对象的真实scaleX值 。
		 * @param object 显示对象。
		 * @param value scaleX值。
		 */	
		public static function scaleX(object:Object, value:Number):void
		{
			object.scaleX = dpiValue(value);
		}
		/**
		 * 设置对象的真实scaleY值 。
		 * @param object 显示对象。
		 * @param value scaleY值。
		 */	
		public static function scaleY(object:Object, value:Number):void
		{
			object.scaleY = dpiValue(value);
		}
		
		/**
		 * 取得真实转化之后的x值。 
		 * @param value x值。
		 * @return 转化之后的值。
		 */		
		public static function getX(value:Number):Number
		{
			return dpiValue(value);
		}
		/**
		 * 取得真实转化之后的y值。 
		 * @param value y值。
		 * @return 转化之后的值。
		 */		
		public static function getY(value:Number):Number
		{
			return dpiValue(value);
		}
		/**
		 * 取得真实转化之后的width值。 
		 * @param value width值。
		 * @return 转化之后的值。
		 */		
		public static function getWidth(value:Number):Number
		{
			return dpiValue(value);
		}
		/**
		 * 取得真实转化之后的height值。 
		 * @param value height值。
		 * @return 转化之后的值。
		 */		
		public static function getHeight(value:Number):Number
		{
			return dpiValue(value);
		}
		/**
		 * 取得真实转化之后的scaleX值。 
		 * @param value scaleX值。
		 * @return 转化之后的值。
		 */		
		public static function getScaleX(value:Number):Number
		{
			return dpiValue(value);
		}
		/**
		 * 取得真实转化之后的scaleY值。 
		 * @param value scaleY值。
		 * @return 转化之后的值。
		 */		
		public static function getScaleY(value:Number):Number
		{
			return dpiValue(value);
		}
		/**
		 * 取得真实转化之后的值。 
		 * @param value 需要转化的值。
		 * @return 转化之后的值。
		 */		
		public static function dpiValue(value:Number):Number
		{
			return value * scale;
		}
		/**
		 * 设置显示对象的真实xy值。 
		 * @param object 显示对象。
		 * @param px x值。
		 * @param py y值。
		 */		
		public static function xy(object:Object, px:Number, py:Number):void
		{
			x(object, px);
			y(object, py);
		}
		/**
		 * 设置显示对象的真实width和height值。 
		 * @param object 显示对象。
		 * @param pw width值。
		 * @param ph height值。
		 */		
		public static function wh(object:Object, pw:Number, ph:Number):void
		{
			width(object, pw);
			height(object, ph);
		}
		/**
		 * 设置显示对象的真实scaleX和scaleY值。 
		 * @param object 显示对象。
		 * @param sx scaleX值。
		 * @param sy scaleY值。
		 */		
		public function scaleXY(object:Object, sx:Number, sy:Number):void
		{
			scaleX(object, sx);
			scaleY(object, sy);
		}
		
		
	}
}