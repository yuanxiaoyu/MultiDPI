package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;
	
	import scalar.core.DPICore;
	
	import starling.core.Starling;
	
	public class MultiDPI extends Sprite
	{
		public function MultiDPI()
		{
			super();
			
			// 支持 autoOrient
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			init();
		}
		
		private function init():void
		{
			var viewPort:Rectangle;
			DPICore.scale = 1;
			switch(DPICore.scale)
			{
				case 1:
					viewPort = new Rectangle(0, 0, 320, 480);
					break;
				case 2:
					viewPort = new Rectangle(0, 0, 640, 960);
					break;
				default:
					break;
			}
			
			var _starling:Starling = new Starling(StarlingMain, stage, viewPort);
			_starling.start();
		}
	}
}